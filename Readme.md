## Table of Contents

- [About](#about)
- [Included](#included)
- [Installation](#installation)

## About

Repository of custom dotfiles and configuration files for personal use

## Included

- .nethackrc (NetHack game config)
- .tmux.conf (tmux config)
- .vimrc (vim config)
- UltiSnips (snippets for vim's ultisnip plugin)
- carlos.itermcolors (colors for iTerm)
- .editorconfig (Configuration for JSBeautify Plugin)

## Installation

Symlink files to the home folder

Vim plugin maneger [Vundle]: 

1. Set up [plug.vim]:

   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

2. Install Plugins:

   Launch `vim` and run `:PlugInstall`

   To install from command line: `vim +PlugInstall`

